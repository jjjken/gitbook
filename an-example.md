# **1​ What is gitbook?**

**GitBook is a tool to create book, public documentation, enterprise manual, thesis, research papers, etc. here is an example of what gitbook look like. As you can see, gitbook is very simple, gitbook actually only have two things : left vertical menu, and right side article.**

123**  
**

**    
**

![](https://lh5.googleusercontent.com/oKM1Pc0Gm4Gs8gebLOdnaR29_iUU455sXcU919RTsH4VJECVIids0iz8lqTUmpiBgTTX7I1SqO1S385ncHXXRZk7-4CC70uxsMQB4c4VLdmIRV9puc864melXKsGYdHXriLGSrms)

**    
**

**You need two things on your computer to edit the Gitbook:**

**    
**

* **Git \(you can use a visual interface such as Gitkraken\)**

* **Gitbook Editor**

**    
**

**Git is used to synchronize your local copy with the server.**

